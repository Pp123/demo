package com.abc1.dao;

import com.abc1.domain.User;
import org.apache.ibatis.annotations.Results;

import java.util.List;

public interface UserMapper {

    List<User> findAll();

    User findById(int id);

    void delete(int id);

    void update(User user);

    void save(User user);

    List<User> findByCondition(User user);

    List<User> findByIds(List<Integer> ids);

    void saveDate(User user);

    List<User> findAllUserAndOrder();

    List<User> findAllUserAndRole();
}
